package kz.aitu.project;


import java.sql.*;
import java.util.Collections;



public class Main {

    static final String DB_URL = "jdbc:postgresql://http://127.0.0.1:56951";
    static final String USER = "postgres";
    static final String PASS = "easports";


    public static void main(String[] args){
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
            return;
        }

        System.out.println("PostgreSQL JDBC Driver successfully connected");
        Connection connection = null;

        try {
            connection = DriverManager
                    .getConnection(DB_URL, USER, PASS);
            Statement statement = connection.createStatement();
            kz.aitu.project.Group group = new Group();
            Student student = new Student();
            ResultSet result_group = statement.executeQuery("select * from groups");
            while(result_group.next()){
                System.out.println(result_group.getString("name") + " " + result_group.getInt("id") + ":");
                group.setId(result_group.getInt("id"));
                group.setName(result_group.getString("name"));
                ResultSet result_student = statement.executeQuery("select * from student");
                while(result_student.next()){
                    student.setName(result_student.getString("name"));
                    student.setGroupId(result_student.getInt("group_id"));
                    if(group.getId() == student.getGroupId()) {
                        System.out.println(student.getName());
                    }
                }
            }

        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
            return;
        }

        if (connection != null) {
            System.out.println("You successfully connected to database now");
        } else {
            System.out.println("Failed to make connection to database");
        }


    }

}
