package kz.aitu.project;


public class Group {
    private int id;
    private String name;

    public Group() {}

    @Override
    public String toString() {
        return String.format(id + " - " + name);
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }
}
